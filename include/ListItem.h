#ifndef EASYMD_MENUITEM_H
#define EASYMD_MENUITEM_H

#include "Paragraph.h"


class ListItem
    : public Paragraph
{
public:
    ListItem(unsigned int index, unsigned int level, bool marked);
    int print(FILE *) const;
    unsigned getLevel() const;
    unsigned getIndex() const;
    bool isMarked() const;
    void addTextLine(const wchar_t *text);
    const wchar_t *outputFormatLine(const wchar_t *begin, const wchar_t *end, unsigned int maxWidth, FILE *file) const;
private:
    wchar_t *mText;
    size_t mSize;
    unsigned mIndex;
    unsigned mLevel;
    bool mMarked;
};

#endif // EASYMD_MENUITEM_H
