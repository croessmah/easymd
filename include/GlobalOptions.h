#ifndef GLOBALOPTIONS_H_INCLUDED
#define GLOBALOPTIONS_H_INCLUDED



void globalSetColored(bool);
void globalSetListMarker(wchar_t);
void globalSetOutputWidth(unsigned int);
void globalSetParagraphBeginMargin(unsigned int);
void globalSetListTextMargin(unsigned int);
void globalSetPrintEndLine(bool);


bool globalGetColored();
wchar_t globalGetListMarker();
unsigned int globalGetOutputWidth();
unsigned int globalGetParagraphBeginMargin();
unsigned int globalGetListTextMargin();
const wchar_t *globalGetEndLine();

#endif // GLOBALOPTIONS_H_INCLUDED
