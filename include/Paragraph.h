#ifndef EASYMD_PARAGRAPH_H
#define EASYMD_PARAGRAPH_H

#include "AbstractText.h"


class Paragraph
    : public AbstractText
{
public:
    Paragraph();
    virtual void addTextLine(const wchar_t *text) = 0;
    unsigned long int getCountSymbols() const;
    unsigned long int getCountWords() const;
protected:
    void setCountSymbols(unsigned long int);
    void setCountWords(unsigned long int);
private:
    unsigned long int mCountSymbols;
    unsigned long int mCountWords;
};

#endif // EASYMD_PARAGRAPH_H
