#ifndef EASYMD_ABSTRACTTEXT_H
#define EASYMD_ABSTRACTTEXT_H

#include <cstdio>

class AbstractText
{
public:
    virtual unsigned long int getCountSymbols() const = 0;
    virtual unsigned long int getCountWords() const = 0;
    virtual int print(FILE *) const = 0;
    virtual ~AbstractText() {}
};

#endif // EASYMD_ABSTRACTTEXT_H
