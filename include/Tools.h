#ifndef EASYMD_TOOLS_H_INCLUDED
#define EASYMD_TOOLS_H_INCLUDED

#include <cstdio>

//Функция поиска пробела
const wchar_t *findSpace(const wchar_t *begin, const wchar_t *end);

//Функция поиска не пробела
const wchar_t *findNonSpace(const wchar_t *begin, const wchar_t *end);

//Функция вычисляет размер строки, учитывая '\0', если он есть.
size_t rangeSize(const wchar_t *begin, const wchar_t *end, bool trim = true);

//Функция копирует последовательность
void rangeCopy(wchar_t *destBegin, const wchar_t *srcBegin, const wchar_t *srcEnd, bool trim);

//Создает копию последовательности
wchar_t *rangeCopyCreate(const wchar_t *begin, const wchar_t *end, size_t &size, bool trim = true);

//Конкатенация двух последовательностей с выделением памяти
wchar_t *concatWithAllocate(const wchar_t *firstBegin,const wchar_t *firstEnd,
    const wchar_t *secondBegin, const wchar_t *secondEnd,
    size_t &size, bool trimFirst, bool trimSecond);

//Функция считае количество слов и символов
void getCountSymbolsAndWords(const wchar_t *begin, const wchar_t *end, 
    unsigned long int *symbols, unsigned long int *words);

//Функция ищет слово, которое нужно перенести либо место переноса.
const wchar_t *checkTextNewLinePosition(const wchar_t *begin, const wchar_t *end, unsigned int maxWidth);

//Функция чтения строки неопределенной длины из файла
wchar_t *readLineAndAllocate(FILE *file, bool bomChecked = false);


void lineConvertAndPrintf(FILE *file, const wchar_t *format, ...);


#endif // EASYMD_TOOLS_H_INCLUDED
