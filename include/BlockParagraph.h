#ifndef EASYMD_BLOCKPARAGRAPH_H
#define EASYMD_BLOCKPARAGRAPH_H

#include "Paragraph.h"


class BlockParagraph
    : public Paragraph
{
public:
    BlockParagraph();
    ~BlockParagraph();
    int print(FILE *) const;
    void addTextLine(const wchar_t *text);
private:
    const wchar_t *outputFormatLine(const wchar_t *begin, const wchar_t *end, unsigned int maxWidth, FILE *file) const;
    wchar_t *mText;
    size_t mSize;
};

#endif // EASYMD_BLOCKPARAGRAPH_H
