#ifndef EASYMD_BLOCKHEADER_H
#define EASYMD_BLOCKHEADER_H

#include "Paragraph.h"


class BlockHeader
    : public Paragraph
{
public:
    BlockHeader();
    int print(FILE *) const;
    void addTextLine(const wchar_t *text);
    ~BlockHeader();
private:
    const wchar_t *outputFormatLine(const wchar_t *begin, const wchar_t *end, unsigned int maxWidth, FILE *file) const;
    wchar_t *mText;
    size_t mSize;
    unsigned int mLevel;
};

#endif // EASYMD_BLOCKHEADER_H
