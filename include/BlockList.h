#ifndef EASYMD_BLOCKLIST_H
#define EASYMD_BLOCKLIST_H

#include "Paragraph.h"

class ListItem;//forward declaration


class BlockList
    : public Paragraph
{
public:
    BlockList();
    void addTextLine(const wchar_t *text);
    int print(FILE *file) const;
    virtual ~BlockList();
    unsigned long int getCountSymbols() const;
    unsigned long int getCountWords() const;
private:
    ListItem **mItems;
    size_t mCountItems;
};

#endif // EASYMD_BLOCKLIST_H
