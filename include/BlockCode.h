#ifndef EASYMD_BLOCKCODE_H
#define EASYMD_BLOCKCODE_H

#include "Paragraph.h"


class BlockCode
    : public Paragraph
{
public:
    BlockCode();
    int print(FILE *) const;
    void addTextLine(const wchar_t *text);
    ~BlockCode();
private:
    wchar_t *mText;
    size_t mSize;
    unsigned int mLevel;
};

#endif // EASYMD_BLOCKCODE_H
