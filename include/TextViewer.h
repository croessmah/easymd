#ifndef EASYMD_TEXTVIEWER_H
#define EASYMD_TEXTVIEWER_H


class AbstractText;
class Paragraph;

class TextViewer
{
public:
    TextViewer();
    ~TextViewer();
    bool loadFromFile(const char *fileName);
    void print(bool isFile) const;
private:
    void currentLineAdd(const wchar_t *textLine);
    void addCurrentToItems();
    Paragraph *mCurrentParagraph;
    AbstractText **mItems;
    unsigned mCountItems;
};

#endif // EASYMD_TEXTVIEWER_H
