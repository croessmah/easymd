#include "GlobalOptions.h"


struct GlobalOptions
{
    GlobalOptions()
        : outputWidth(100)
        , paragraphBeginMargin(4)
        , listTextMargin(1)
        , listMarker(L'*')
        , colored(true)
        , printEndLine(false)
    {
    }
    unsigned int outputWidth;
    unsigned int paragraphBeginMargin;
    unsigned int listTextMargin;
    wchar_t listMarker;
    bool colored;
    bool printEndLine;
};


static GlobalOptions globalOptions;


void globalSetColored(bool value)
{
    globalOptions.colored = value;
}

void globalSetListMarker(wchar_t value)
{
    globalOptions.listMarker = value;
}

void globalSetOutputWidth(unsigned int value)
{
    globalOptions.outputWidth = value;
}

void globalSetParagraphBeginMargin(unsigned int value)
{
    globalOptions.paragraphBeginMargin = value;
}


void globalSetListTextMargin(unsigned int value)
{
    globalOptions.listTextMargin = value;
}

bool globalGetColored()
{
    return globalOptions.colored;
}


wchar_t globalGetListMarker()
{
    return globalOptions.listMarker;
}


void globalSetPrintEndLine(bool value)
{
    globalOptions.printEndLine = value;
}



unsigned int globalGetOutputWidth()
{
    return globalOptions.outputWidth;
}


unsigned int globalGetParagraphBeginMargin()
{
    return globalOptions.paragraphBeginMargin;
}

unsigned int globalGetListTextMargin()
{
    return globalOptions.listTextMargin;
}


const wchar_t *globalGetEndLine()
{
    return globalOptions.printEndLine?L"\n":L"";
}
