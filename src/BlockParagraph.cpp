#include "BlockParagraph.h"
#include "Tools.h"
#include "GlobalOptions.h"
#include <cstring>
#include <cctype>
#include <cwchar>

BlockParagraph::BlockParagraph()
    : mText(0)
    , mSize(0)
{
}


void BlockParagraph::addTextLine(const wchar_t *text)
{
    size_t newSize = 0;
    size_t sourceStrLen = wcslen(text);
    //Заменяем нулевой символ на перенос строки
    if (sourceStrLen == 0) {
        return;
    }
    if (mText && mSize) {
        mText[mSize - 1] = L'\n';
    }
    //Объединяем две строки
    wchar_t *safeText = concatWithAllocate(mText, mText + mSize, text, text + sourceStrLen + 1, newSize, false, true);

    delete []mText;//очищаем память
    //Обновляем значения переменных
    mText = safeText;
    mSize = newSize;
    //Вычисляем количество слов и символов
    unsigned long int countWords = 0;
    unsigned long int countSymbols = 0;

    getCountSymbolsAndWords(text, text + sourceStrLen, &countSymbols, &countWords);
    setCountSymbols(getCountSymbols() + countSymbols);
    setCountWords(getCountWords() + countWords);
}



int BlockParagraph::print(FILE *file) const
{
    if (mText == NULL || mSize == 0) {
        return -1;
    }

    unsigned int width = globalGetOutputWidth();
    unsigned int firstLineSpacing = globalGetParagraphBeginMargin();

    //формируем строку формата для "красной строки"
    wchar_t formatBuffer[64];
    swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", firstLineSpacing, L"ls");
    //выводим красную строку
    lineConvertAndPrintf(file, formatBuffer, L"");
    //определяем границы последовательности
    const wchar_t *begin = mText;
    const wchar_t *end = (mText + mSize) - 1;
    unsigned int firstLineWidth = width - firstLineSpacing;
    //выводим первую строку
    begin = outputFormatLine(begin, end, firstLineWidth, file);
    lineConvertAndPrintf(file, L"%ls", globalGetEndLine());
    //выводим остальные строки
    while (begin != end) {
        begin = outputFormatLine(begin, end, width, file);
        lineConvertAndPrintf(file, L"%ls", globalGetEndLine());
    }


    return 0;
}
//lineConvertAndPrintf
//вывод одной строки
const wchar_t *BlockParagraph::outputFormatLine(
    const wchar_t *begin,
    const wchar_t *end,
    unsigned int maxWidth,
    FILE *file
) const {
    //буфер для формата
    wchar_t formatBuffer[64];
    //Ищем первое слово
    begin = findNonSpace(begin, end);
    //Определяем где закончится строка при выводе в поле заданной ширины
    const wchar_t *endLineCheck = checkTextNewLinePosition(begin, end, maxWidth);
    //Формируем строку формата
    swprintf(formatBuffer, 64, L"%ls%u%ls", L"%.", static_cast<unsigned int>(endLineCheck - begin), L"ls");
    //Выводим символы
    lineConvertAndPrintf(file, formatBuffer, begin);
    //Уменьшаем оставшуюся ширину
    maxWidth -= static_cast<unsigned int>(endLineCheck - begin);
    //Если чтение завершено на пробеле или данные закончились, то
    if (endLineCheck == end || isspace(*endLineCheck)) {
        swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", maxWidth, L"ls");
        lineConvertAndPrintf(file, formatBuffer, L"");
        begin = findNonSpace(endLineCheck, end);//пропускаем все пробелы
    } else {
        //Определяем оставшуюся ширину
        begin = endLineCheck;
        if (maxWidth < 3) {//Если осталось меньше трех символов
            swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", maxWidth, L"ls");
            lineConvertAndPrintf(file, formatBuffer, L"");
            return begin;//и выходим из функции
        }
        --maxWidth;//резервируем место под черточку
        size_t wordSize = findSpace(endLineCheck, end) - endLineCheck;
        //printf("\nCROP: %d\n", cropWidth);
        if (wordSize > 3 && (wordSize - maxWidth) > 1){
            swprintf(formatBuffer, 64, L"%ls%u%ls-", L"%.", maxWidth, L"ls");
            lineConvertAndPrintf(file, formatBuffer, begin);
            begin += maxWidth;
        } else {
            swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", maxWidth + 1, L"ls");
            lineConvertAndPrintf(file, formatBuffer, L"");
        }
    }
    return begin;
}


BlockParagraph::~BlockParagraph()
{
    delete[] mText;
}
