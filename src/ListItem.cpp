﻿#include "ListItem.h"
#include "Tools.h"
#include "GlobalOptions.h"
#include <cstring>
#include <cctype>
#include <cwchar>

ListItem::ListItem(unsigned int index, unsigned int level, bool marked)
    : mText(0)
    , mSize(0)
    , mIndex(index)
    , mLevel(level)
    , mMarked(marked)
{
}

unsigned ListItem::getLevel() const
{
    return mLevel;
}

unsigned ListItem::getIndex() const
{
    return mIndex;
}

bool ListItem::isMarked() const
{
    return mMarked;
}
void ListItem::addTextLine(const wchar_t *text)
{
    size_t newSize = 0;
    size_t sourceStrLen = wcslen(text);
    //Заменяем нулевой символ на перенос строки
    if (sourceStrLen == 0) {
        return;
    }
    if (mText && mSize) {
        mText[mSize - 1] = L'\n';
    }
    //Объединяем две строки
    wchar_t *safeText = concatWithAllocate(mText, mText + mSize, text, text + sourceStrLen + 1, newSize, false, true);

    delete []mText;//очищаем память
    //Обновляем значения переменных
    mText = safeText;
    mSize = newSize;
    //Вычисляем количество слов и символов
    unsigned long int countWords = 0;
    unsigned long int countSymbols = 0;
    getCountSymbolsAndWords(text, text + sourceStrLen, &countSymbols, &countWords);
    setCountSymbols(getCountSymbols() + countSymbols);
    setCountWords(getCountWords() + countWords);
}



int ListItem::print(FILE *file) const
{
    unsigned int width = globalGetOutputWidth();
    unsigned itemMargin = mLevel * 2 + globalGetListTextMargin();
    unsigned lineWidth = width - itemMargin;
    wchar_t * itemMarginFormat = new wchar_t [itemMargin + 1];
    for(unsigned i = 0; i < itemMargin; ++i) {
        itemMarginFormat[i] = L' ';
    }
    itemMarginFormat[itemMargin] = L'\0';


    //определяем границы последовательности
    const wchar_t *begin = mText;
    const wchar_t *end = (mText + mSize) - 1;
    //выводим первую строку
    for(unsigned i = 0; i < mLevel; ++i) {
        lineConvertAndPrintf(file, L"  ");
    }

    unsigned markerLength = 2;

    if (globalGetColored()) {//если установлен цветной вывод
        fprintf(file, "%s", "\x1b[36m");//Устанавливаем голубой цвет
    }
    
        
    if (mMarked) {
        lineConvertAndPrintf(file, L"%lc%ls", globalGetListMarker(), L" ");
    } else {
        wchar_t indexBuffer[16];
        swprintf(indexBuffer, 16, L"%u", mIndex);       
        markerLength = wcslen(indexBuffer) + 2;
        lineConvertAndPrintf(file, L"%ls. ", indexBuffer);
    }
    
    if (globalGetColored()) {//если установлен цветной вывод
        fprintf(file, "%s", "\e[0m");//Восстанавливаем цвет
    }
    
    unsigned firstLineWidth = width - (mLevel * 2 + markerLength);
    //Выводим первую строку
    begin = outputFormatLine(begin, end, firstLineWidth, file);
    lineConvertAndPrintf(file, L"%ls", globalGetEndLine());
    //выводим остальные строки
    while (begin != end) {
        lineConvertAndPrintf(file, L"%ls", itemMarginFormat);
        begin = outputFormatLine(begin, end, lineWidth, file);
        lineConvertAndPrintf(file, L"%ls", globalGetEndLine());
    }

    return 0;
}


//вывод одной строки
const wchar_t *ListItem::outputFormatLine(
    const wchar_t *begin,
    const wchar_t *end,
    unsigned int maxWidth,
    FILE *file
) const {
    //буфер для формата
    wchar_t formatBuffer[64];
    //Ищем первое слово
    begin = findNonSpace(begin, end);
    //Определяем где закончится строка при выводе в поле заданной ширины
    const wchar_t *endLineCheck = checkTextNewLinePosition(begin, end, maxWidth);
    //Формируем строку формата
    swprintf(formatBuffer, 64, L"%ls%u%ls", L"%.", static_cast<unsigned int>(endLineCheck - begin), L"ls");
    //Выводим символы
    lineConvertAndPrintf(file, formatBuffer, begin);
    //Уменьшаем оставшуюся ширину
    maxWidth -= static_cast<unsigned int>(endLineCheck - begin);
    //Если чтение завершено на пробеле или данные закончились, то
    if (endLineCheck == end || isspace(*endLineCheck)) {
        swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", maxWidth, L"ls");
        lineConvertAndPrintf(file, formatBuffer, "");
        begin = findNonSpace(endLineCheck, end);//пропускаем все пробелы
    } else {
        //Определяем оставшуюся ширину
        begin = endLineCheck;
        if (maxWidth < 3) {//Если осталось меньше трех символов
            swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", maxWidth, L"ls");
            lineConvertAndPrintf(file, formatBuffer, L"");
            return begin;//и выходим из функции
        }
        --maxWidth;//резервируем место под черточку
        size_t wordSize = findSpace(endLineCheck, end) - endLineCheck;
        if (wordSize > 3 && (wordSize - maxWidth) > 1){
            swprintf(formatBuffer, 64, L"%ls%u%ls-", L"%.", maxWidth, L"ls");
            lineConvertAndPrintf(file, formatBuffer, begin);
            begin += maxWidth;
        } else {
            swprintf(formatBuffer, 64, L"%ls%u%ls", L"%", maxWidth + 1, L"ls");
            lineConvertAndPrintf(file, formatBuffer, L"");
        }
    }
    return begin;
}
