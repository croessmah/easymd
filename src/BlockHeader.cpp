﻿#include "BlockHeader.h"
#include "Tools.h"
#include "GlobalOptions.h"
#include <cstring>
#include <stdexcept>
#include <cwctype>

BlockHeader::BlockHeader()
    : mText(0)
    , mSize(0)
    , mLevel(0)
{
}


void BlockHeader::addTextLine(const wchar_t *text)
{
    //Вычисляем уровень заголовка
    mLevel = 0;
    for(;*text && *text == L'#'; ++text) {
        ++mLevel;
    }
    size_t sourceStrLen = wcslen(text);//Определяем размер переданной строки

    delete []mText;
    mText = NULL;
    size_t size = 0;
    //Создаем копию строки без лишних пробелов
    mText = rangeCopyCreate(text, text + sourceStrLen + 1, size, true);
    mSize = size;
    unsigned long int countWords = 0;
    unsigned long int countSymbols = 0;
    getCountSymbolsAndWords(text, text + sourceStrLen, &countSymbols, &countWords);
    setCountSymbols(countSymbols);
    setCountWords(countWords);
}



int BlockHeader::print(FILE *file) const
{
    //todo: цветной вывод
    if (!mText) {
        return 0;
    }
    size_t fullWidth = globalGetOutputWidth();

    if (fullWidth < (mLevel * 2 + 1)) {
        throw std::runtime_error("invalid word width");
        return -1;
    }

    size_t outputWidth = fullWidth - mLevel * 2;

    //Генерация первой строки с решетками
    wchar_t *sharpFirstLine = new wchar_t[fullWidth+1];
    //memset(sharpFirstLine, '#', fullWidth);
    for (size_t i = 0; i < fullWidth; ++i) {
        sharpFirstLine[i] = L'#';
    }
    sharpFirstLine[fullWidth] = L'\0';
    
    if (globalGetColored()) {//Если установлен цветной вывод, то
        fprintf(file, "%s", "\x1b[33m");//Устанавливаем желтый цвет
    }
    
    //Вывод первой линии из решеток
    
    lineConvertAndPrintf(file, L"%ls", sharpFirstLine);
    //Генерация начала строки заголовка
    wchar_t *sharpLine = new wchar_t[mLevel + 1];
    for (size_t i = 0; i < mLevel; ++i) {
        sharpLine[i] = L'#';
    }
    sharpLine[mLevel] = L'\0';


    try{
        const wchar_t *begin = mText;
        const wchar_t *end = (mText + mSize) - 1;
        //Вывод строк заголовка по центру
        while (begin != end) {
            lineConvertAndPrintf(file, L"%ls%ls", globalGetEndLine(), sharpLine);//Вывод начальных решеток
            const wchar_t *line = outputFormatLine(begin, end, outputWidth, file);
            lineConvertAndPrintf(file, L"%ls", sharpLine);//Вывод конечных решеток
            begin = line;
            //begin = outputFormatLine(begin, end, outputWidth, file);//Вывод текста по центру ширины вывода
            //fprintf(file, "%s\n", sharpLine);//Вывод конечных решеток
        }
        //Вывод последней линии из решеток
        lineConvertAndPrintf(file, L"%ls%ls%ls", globalGetEndLine(), sharpFirstLine, globalGetEndLine());
        delete[] sharpFirstLine;
        delete[] sharpLine;
        if (globalGetColored()) {//Если установлен цветной вывод, то
            fprintf(file, "%s", "\e[0m");//Восстановливаем желтый цвет
        }
    } catch (...) {
        if (globalGetColored()) {//Если установлен цветной вывод, то
            fprintf(file, "%s", "\e[0m");//Восстановливаем желтый цвет
        }
        delete[] sharpFirstLine;
        delete[] sharpLine;
        throw;
    }   
    

    return 0;
}



const wchar_t *BlockHeader::outputFormatLine(const wchar_t *begin, const wchar_t *end, unsigned int maxWidth, FILE *file) const
{
    wchar_t formatBuffer[128];
    const wchar_t *endLineCheck = checkTextNewLinePosition(begin, end, maxWidth);
    unsigned lineWidth = static_cast<unsigned int>(endLineCheck - begin);
    unsigned leftSkip = (maxWidth - lineWidth) / 2;
    unsigned rightSkip = (maxWidth - lineWidth) - leftSkip;
    if (endLineCheck == end || iswspace(*endLineCheck)) {
        swprintf(formatBuffer, 128, L"%ls%u%ls%ls%u%ls%ls%u%ls", L"%", leftSkip, L"ls", L"%.", lineWidth, L"ls", L"%", rightSkip, L"ls");
        //%2s%.5s%3s
        lineConvertAndPrintf(file, formatBuffer, L"", begin, L"");
        //"  fffff   "
        begin = findNonSpace(endLineCheck, end);
    } else {
        unsigned int cropWidth = lineWidth + 1;
        if (maxWidth < cropWidth) {
            swprintf(formatBuffer, 128, L"%ls%u%ls%ls%u%ls%ls%u%ls", L"%", leftSkip, L"ls", L"%.", lineWidth, L"ls", L"%", rightSkip, L"ls");
            lineConvertAndPrintf(file, formatBuffer, L"", begin, L"");
            return endLineCheck;
        }
        cropWidth = maxWidth - cropWidth;
        if (cropWidth > 1) {
            lineWidth += cropWidth;//вычисляем ширину строки
            leftSkip = (maxWidth - lineWidth) / 2;//вычисляем количество пробелов слева
            rightSkip = (maxWidth - lineWidth) - leftSkip;//вычисляем количество пробелов справа
            swprintf(formatBuffer, 128, L"%ls%u%ls%ls%u%ls%ls%ls%u%ls", L"%", leftSkip, L"ls", L"%.", lineWidth, L"ls", L"-", L"%", rightSkip - 1, L"ls");
            //%0ls%.9ls-%0ls
            lineConvertAndPrintf(file, formatBuffer, L"", begin, L"");
            begin += lineWidth;
        } else {
            swprintf(formatBuffer, 128, L"%ls%u%ls%ls%u%ls%ls%u%ls", L"%", leftSkip + 1, L"ls", L"%.", lineWidth, L"ls", L"%", rightSkip - 1, L"ls");
            lineConvertAndPrintf(file, formatBuffer, L"", begin, L"");
            begin = endLineCheck;
        }
    }
    return begin;
}


BlockHeader::~BlockHeader()
{
    delete[] mText;
}
