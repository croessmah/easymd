#include "Paragraph.h"



Paragraph::Paragraph()
    : mCountSymbols(0)
    , mCountWords(0)
{
}


unsigned long int Paragraph::getCountSymbols() const
{
    return mCountSymbols;
}

unsigned long int Paragraph::getCountWords() const
{
    return mCountWords;
}


void Paragraph::setCountSymbols(unsigned long int value)
{
    mCountSymbols = value;
}


void Paragraph::setCountWords(unsigned long int value)
{
    mCountWords = value;
}
