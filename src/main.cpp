#include <iostream>

#include "TextViewer.h"
#include "GlobalOptions.h"



#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include <cstring> 
#include <cstdio>
#include <cstdlib>
#include <clocale>


void printHelpScreen()
{
    fprintf(stdout, "Help.\nUsage: ./easymd -f file_name [-t list margin] [-m list marker] [-r paragraph margin]\n");
    fprintf(stdout, "-h - print this screen\n");
    fprintf(stdout, "-v - programm version\n");
}


int main(int argc, char ** argv)
{
    setlocale(LC_ALL, "en_US.UTF-8");
    int optCh = 0;
    unsigned int mrg = 0;
    char inputFileName[FILENAME_MAX + 1] = {0};
    while((optCh = getopt(argc, argv, ":f:t:m:r:hv")) != -1) {
        switch(optCh) {
        case 'f':
            strcpy(inputFileName, optarg);
            break;
        case 't':
            mrg = atoi(optarg);
            if (mrg > globalGetOutputWidth() / 2) {
                fprintf(stderr, "List margin is invalid\n");
                return -1;
            }
            globalSetListTextMargin(mrg);
            break;
        case 'm':
            mrg = strlen(optarg);
            if (!(mrg == 1)) {
                fprintf(stderr, "List marker is invalid\n");
                return -1;
            }
            globalSetListMarker(*optarg);
            break;
        case 'r':
            mrg = atoi(optarg);
            if (mrg > globalGetOutputWidth() / 2) {
                fprintf(stderr, "Paragraph margin is invalid\n");
                return -1;
            }
            globalSetParagraphBeginMargin(mrg);
            break;
        case 'h':
            printHelpScreen();
            return 0;
            break;
        case 'v':
            fprintf(stdout, "name: \"%s\" version: %s\n", "easymd", "0.0.1");
            return 0;
            break;
        case ':':
            fprintf(stderr, "%s: option '-%c' requers an argument\n", argv[0], optopt);
            printHelpScreen();
            return -1;
            break;
        case '?':
        default:
            fprintf(stderr, "%s: option '-%c' is invalid\n", argv[0], optopt);
            printHelpScreen();
            return -1;
            break;
        }
    }

    if (*inputFileName == '\0') {
        fprintf(stderr, "File name not set\n");
        printHelpScreen();
        return -1;
    }

    winsize winSize;
    memset(&winSize, 0, sizeof(winSize));//Обнуляем переменную
    bool isFile = false;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &winSize);//запрос размеров терминала
    if (isatty(fileno(stdout))) {//Если вывод идет в терминал, то
        globalSetColored(true);//Устанавливаем цветной вывод, и
        globalSetOutputWidth(winSize.ws_col);//Устанавливаем ширину вывода
    } else {//иначе
        globalSetColored(false);//Отключаем цветной вывод
        globalSetOutputWidth(100);//Устанавливаем ширину вывода равной 100
        globalSetPrintEndLine(true);//Добавляем флаг печати символа '\n'
        isFile = true;
    }
    
    
    TextViewer viewer;
    if (viewer.loadFromFile(inputFileName)) {
        viewer.print(isFile);
        return 0;
    }
    return -1;
}
