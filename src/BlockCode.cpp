﻿#include "BlockCode.h"
#include "Tools.h"
#include "GlobalOptions.h"
#include <cstring>
#include <stdexcept>


//Функция поиска '\n'
static const wchar_t *findNextLine(const wchar_t *begin, const wchar_t *end)
{
    while (begin != end && *begin != L'\n') {
        ++begin;
    }
    return begin;
}



BlockCode::BlockCode()
    : mText(0)
    , mSize(0)
{
}


void BlockCode::addTextLine(const wchar_t *text)
{
    size_t newSize = 0;

    if (*text == L'\t') {
        ++text;
    } else {
        size_t spacesCount = 0;
        while (*text == ' ') {
            ++text;
            ++spacesCount;
        }
        if (spacesCount < 4) {
            throw std::runtime_error("Wrong code format: spaces count is less 4");
        }
    }

    size_t sourceStrLen = wcslen(text);
    if (sourceStrLen == 0) {
        text = L"";
        sourceStrLen = 0;
    }

    if (mText && mSize) {
        mText[mSize - 1] = L'\n';
    }

    wchar_t *safeText = concatWithAllocate(mText, mText + mSize, text, text + sourceStrLen + 1, newSize, false, false);
    
    delete []mText;
    mText = safeText;
    mSize = newSize;
    unsigned long int countWords = 0;
    unsigned long int countSymbols = 0;
    getCountSymbolsAndWords(text, text + sourceStrLen, &countSymbols, &countWords);
    setCountSymbols(getCountSymbols() + countSymbols);
    setCountWords(getCountWords() + countWords);
}




int BlockCode::print(FILE *file) const
{
    if (mText == NULL || mSize == 0) {
        return -1;
    }
    unsigned int fullWidth = globalGetOutputWidth();//Полная ширина вывода
    if (fullWidth < 4) {
        throw std::runtime_error("invalid width");
    }
    unsigned int outputWidth = fullWidth - 4;//Остаток ширины для вывода текста
    const wchar_t *begin = mText;
    const wchar_t *end = (mText + mSize) - 1;
    
    if (globalGetColored()) {//Если установлен цветной вывод, то
        fprintf(file, "%s", "\x1b[32m");//Устанавливаем зеленый цвет
    }

    wchar_t formatBuffer[64];
    while (begin != end) {
        const wchar_t *nextLine = findNextLine(begin, end);
        unsigned lineWidth = static_cast<unsigned>(nextLine - begin);
        lineWidth = (lineWidth < outputWidth)?lineWidth:outputWidth;
        lineConvertAndPrintf(file, L"%ls", L"@   ");//Выводим текст в соответствии с форматом

        swprintf(formatBuffer, 64, L"%ls%u%ls%ls%u%ls%ls", L"%.", lineWidth, L"ls", L"%", outputWidth - lineWidth, L"ls", globalGetEndLine());
        lineConvertAndPrintf(file, formatBuffer, begin, L"");
        //std::wcerr << "\n[FORMAT: " << formatBuffer << "\nTEXT: " << begin << "]\n";
        begin = nextLine;//Сдвигаемся на конец строки
        if (begin != end) {//Если это не конец текста
            ++begin;//Пропускаем символ переноса строки
        }
    }
    
    if (globalGetColored()) {//Если установлен цветной вывод, то
        fprintf(file, "%s", "\e[0m");//восстанавливаем цвет
    }
    
    return 0;
}


BlockCode::~BlockCode()
{
    delete[] mText;
}
