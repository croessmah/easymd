#include "Tools.h"
#include "GlobalOptions.h"
#include <cstddef>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <stdexcept>
#include <cwctype>
#include <iostream> //todo: delete this

//Функция поиска пробела
const wchar_t *findSpace(const wchar_t *begin, const wchar_t *end)
{
    for (; begin != end && !iswspace(*begin); ++begin) {//цикл по всей последовательности либо до пробела
    }
    return begin;
}

//Функция поиска не пробела
const wchar_t *findNonSpace(const wchar_t *begin, const wchar_t *end)
{
    for (; begin != end && iswspace(*begin); ++begin) {
    }
    return begin;
}

//Функция вычисляет размер строки, учитывая '\0', если он есть.
size_t rangeSize(const wchar_t *begin, const wchar_t *end, bool trim)
{
    if (!trim) {//если не нужно удалять лишние пробелы
        return static_cast<size_t>(end - begin);//то просто возвращаем длину последовательности
    }
    size_t size = 0;

    begin = findNonSpace(begin, end);//ищем первое слово
    while (begin != end) {
        const wchar_t *firstSpace = findSpace(begin, end);//ищем конец слова
        size += (firstSpace - begin);//прибавляем размер слова к размеру последовательности
        begin = findNonSpace(firstSpace, end);//ищем следующее слово
        if (begin != end && *begin) {//Если еще еесть слова, то
            ++size;//учитываем пробел между словами
        }
    }
    return size;//Возвращаем размер
}


//Функция копирует последовательность
void rangeCopy(wchar_t *destBegin, const wchar_t *srcBegin, const wchar_t *srcEnd, bool trim)
{
    if (trim) {//Если нужно обрезать пробелы
        srcBegin = findNonSpace(srcBegin, srcEnd);//ищется первое слово
        for (; srcBegin != srcEnd; ++destBegin) {//Проходим по всей последовательности
            *destBegin = *srcBegin;//копируем символ в выходную последовательность
            if (iswspace(*srcBegin)) {//если пробельный символ
                srcBegin = findNonSpace(srcBegin, srcEnd);//пропускаем все пробелы
                //...
            } else {//иначе
                ++srcBegin;//сдвигаем итератор на следующий символ
            }
        }
    } else {
        for (; srcBegin != srcEnd; ++srcBegin, ++destBegin) {//копируем все символы в конечную последовательность
            *destBegin = *srcBegin;
        }
    }

}


//Создает копию последовательности
wchar_t *rangeCopyCreate(const wchar_t *begin, const wchar_t *end, size_t &size, bool trim)
{
    size = rangeSize(begin, end, trim);
    wchar_t *result = new wchar_t[size + 1];
    rangeCopy(result, begin, end, trim);
    return result;
}

//Конкатенация двух последовательностей с выделением памяти
wchar_t *concatWithAllocate(
    const wchar_t *firstBegin,
    const wchar_t *firstEnd,
    const wchar_t *secondBegin,
    const wchar_t *secondEnd,
    size_t &size,
    bool trimFirst,
    bool trimSecond)
{
    size_t firstSize = rangeSize(firstBegin, firstEnd, trimFirst);//Вычисляем размер первой последовательности
    size_t secondSize = rangeSize(secondBegin, secondEnd, trimSecond);//Вычисляем размер второй последовательности
    size = firstSize + secondSize;//Вычисляем общий размер новой последовательности
    wchar_t *result = new wchar_t[size + 1];//Выделяем память

    if (firstSize != 0) {//Если первая последовательность не пуста, то
        rangeCopy(result, firstBegin, firstEnd, trimFirst);//Копируем первую последовательность в конечную
    }
    if (secondSize != 0) {//Если вторая последовательность не пуста, то
        rangeCopy(result + firstSize, secondBegin, secondEnd, trimSecond);//Копируем вторую последовательность в конечную, туда, где закончилась первая
    }
    return result;
}


//Функция считае количество слов и символов
void getCountSymbolsAndWords(const wchar_t *begin, const wchar_t *end, unsigned long int *symbols, unsigned long int *words)
{
    *symbols = *words = 0;//обнуляем переданные значения
    begin = findNonSpace(begin, end);//ищем первое слово
    while (begin != end) {
        const wchar_t *firstSpace = findSpace(begin, end);//ищем конец слова
        *symbols += (firstSpace - begin);//Добавляем к количеству символов размер слова
        ++*words;//Увеличиваем кол-во слов
        begin = findNonSpace(firstSpace, end);//ищем следующее слово
        if (begin != end) {
            ++*symbols;
        }
    }
}

//Функция ищет слово, которое нужно перенести либо место переноса.
const wchar_t *checkTextNewLinePosition(const wchar_t *begin, const wchar_t *end, unsigned int maxWidth)
{
    unsigned int width = maxWidth;
    begin = findNonSpace(begin, end);//Ищем первое слово
    while (begin != end) {

        //ищем конец слова
        const wchar_t *firstSpace = findSpace(begin, end);
        unsigned int wordWidth = firstSpace - begin;
        //смотрим ширину слова
        if (wordWidth > maxWidth) {//если она больше ширины поля,
            std::runtime_error("Wrong word length");
        }
        //если не хватает ширины ввода
        if (width < wordWidth) {
            return begin;
        }
        //если слово заканчивается переносом строки или это вообще конец текста
        if (firstSpace == end || *firstSpace == L'\n' || *firstSpace == L'\r') {
            return firstSpace;//то возвращаем найденую позицию как конец строки
        }
        width -= wordWidth;//Вычитаем ширину слова из оставшейся ширины

        if (width == 0) {
            return firstSpace;
        }
        begin = findNonSpace(firstSpace, end);//ищем следующее слово
        if (begin != end) {//если слово не было последним, то
            --width;//вычитаем пробел из ширины слова
        }
    }
    return begin;
}




wchar_t *readLineAndAllocate(FILE *file, bool bomChecked)
{
    char *utf8String = NULL;
    unsigned utf8StringLength = 0;
    const unsigned readBufferSize = 1024;
    char readBuffer[readBufferSize];
    unsigned readLength = 0;

    do {
        if (fgets(readBuffer, readBufferSize, file) == NULL) {
            break;
        }
      
        readLength = strlen(readBuffer);  
                      
        char *newString = new char [utf8StringLength + readLength + 1];
        
        if (utf8String) {
            strcpy(newString, utf8String);
            delete[] utf8String;
        }

        strcpy(newString + utf8StringLength, readBuffer);
        utf8StringLength += readLength;
        utf8String = newString;
    } while (readLength == (readBufferSize - 1) && readBuffer[readBufferSize - 2] != '\n');
    if (utf8String == NULL) {
        return NULL;
    }
    //Если строка не пустая и в конце имеется символ переноса строки, то удаляем символы переноса строки
    if (utf8String[utf8StringLength - 1] == '\n') {
        utf8String[utf8StringLength - 1] = '\0';
    }
    for (char *dst = utf8String, *src = utf8String; (*dst = *src) != '\0'; ++dst, ++src) {
        if (*src == '\r') {
            *dst = ' ';
        }
    }
    std::mbstate_t state = std::mbstate_t();
    const char *beginCopy = utf8String;
    if (bomChecked) {
        unsigned char *utf8Bytes = (unsigned char*)utf8String;
        if (utf8StringLength > 2 && utf8Bytes[0] == 0xEF && utf8Bytes[1] == 0xBB && utf8Bytes[2] == 0xBF) {
            beginCopy += 3;
        }
    }

    const char *p = beginCopy;
    size_t resultBufferSize = mbsrtowcs(NULL, &p, 0, &state) + 1;
    wchar_t *resultString = new wchar_t[resultBufferSize];
    p = beginCopy;
    mbsrtowcs(resultString, &p, resultBufferSize, &state);
    delete []utf8String;
    return resultString;
}


void lineConvertAndPrintf(FILE *file, const wchar_t *format, ...)
{
    const size_t stringBufferSize = globalGetOutputWidth() + 16;
    wchar_t *stringBuffer = new wchar_t[stringBufferSize];
    va_list args;
    va_start(args, format);  
    vswprintf(stringBuffer, stringBufferSize, format, args);
    va_end(args);
    
    const wchar_t *pBuffer = stringBuffer;
    std::mbstate_t state = std::mbstate_t();
    size_t utf8Length = wcsrtombs(NULL, &pBuffer, 0, &state);
    pBuffer = stringBuffer;
    state = std::mbstate_t();
    char *utf8String = new char[utf8Length + 1];
    wcsrtombs(utf8String, &pBuffer, utf8Length + 1, &state);
    fprintf(file, "%s", utf8String);
    delete[] stringBuffer;  
    delete[] utf8String;  
}
