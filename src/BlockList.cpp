﻿#include "BlockList.h"
#include "Tools.h"
#include "GlobalOptions.h"
#include <stdexcept>
#include <cstring>
#include <cwctype>

#include "ListItem.h"

BlockList::BlockList()
    : mItems(0)
    , mCountItems(0)
{
}



BlockList::~BlockList()
{
    for (unsigned i = 0; i < mCountItems; ++i) {
        delete mItems[i];
    }
    delete []mItems;
}


void BlockList::addTextLine(const wchar_t *text)
{
    size_t spaceCount = 0;//Количество пробелов вначале
    for (; *text && *text == L' '; ++text) {
        ++spaceCount;
    }
    unsigned level = spaceCount / 2;//Вычисляем уровень списка
    if (mCountItems == 0 && spaceCount != 0) {//Если список пуст и уровень не равен нулю, то
        throw std::runtime_error("Wrong list format: first item level failed");
    }
    const wchar_t *itemFormat = text;
    bool isNewItem = false;
    bool isMarked = false;
    if (iswdigit(*itemFormat)) {//Если после пробелов идет цифра, то
        ++itemFormat;
        while (iswdigit(*itemFormat)) {//пропускаем все цифры
            ++itemFormat;
        }
        //Если после всех цифр идет точка и пробел,
        if (
            *itemFormat == '.'
            && iswspace(*(itemFormat + 1))
        ) {
            isNewItem = true;//то это новый пункт списка
            ++itemFormat;
        }
    } else {
        //если после пробелов идет "* ",
        if (*(itemFormat + 1) && *itemFormat == L'*' && iswspace(*(itemFormat + 1))) {
            isNewItem = true;//то это новый пункт списка
            isMarked = true;
            ++itemFormat;
        }
    }
    if (isNewItem) {//Если это новый пункт списка
        //Если это не первый пункт списка и уровень больше предыдущего более чем на 1,
        if (
            mItems != NULL
            && level > (mItems[mCountItems - 1]->getLevel() + 1)
        ) {
            throw std::runtime_error("Wrong list format: sublist level is invalid");//выбрасываем исключение
        }
        text = itemFormat;

        ListItem **newItems = new ListItem*[mCountItems + 1];
        if (mItems == NULL) {
            mItems = newItems;
            mItems[0] = new ListItem(1, 0, isMarked);
            mCountItems = 1;
        } else {
            memcpy(newItems, mItems, mCountItems * sizeof(*mItems));
            delete []mItems;
            mItems = newItems;
            ++mCountItems;
            unsigned newIndex = 1;
            for (unsigned i = mCountItems - 1; i != 0; --i) {
                if (mItems[i - 1]->getLevel() < level) {
                    break;
                }
                if (level == mItems[i - 1]->getLevel()) {
                    if (isMarked != mItems[i - 1]->isMarked()) {
                        break;
                    }
                    newIndex = mItems[i - 1]->getIndex() + 1;
                    break;
                }
            }
            mItems[mCountItems - 1] = new ListItem(newIndex, level, isMarked);
        }
    }
    mItems[mCountItems - 1]->addTextLine(text);
}



unsigned long int BlockList::getCountSymbols() const
{
    unsigned long int count = 0;
    for (unsigned i = 0; i < mCountItems; ++i) {
        count += mItems[i]->getCountSymbols();
    }
    return count;
}

unsigned long int BlockList::getCountWords() const
{
    unsigned long int count = 0;
    for (unsigned i = 0; i < mCountItems; ++i) {
        count += mItems[i]->getCountWords();
    }
    return count;
}

int BlockList::print(FILE *file) const
{
    for (unsigned i = 0; i < mCountItems; ++i) {
        mItems[i]->print(file);
    }
    return 0;
}
