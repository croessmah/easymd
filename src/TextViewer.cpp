#include "TextViewer.h"
#include "BlockCode.h"
#include "BlockHeader.h"
#include "BlockList.h"
#include "BlockParagraph.h"

#include "Tools.h"
#include "GlobalOptions.h"

#include <cstring>
#include <cstdio>
#include <stdexcept>
#include <cwctype>

bool isEmptyString(const wchar_t *line)
{
    while (iswspace(*line)) {
        ++line;
    }
    return *line == L'\0';
}



bool isList(const wchar_t *line)
{
    while (iswspace(*line)) {
        ++line;
    }
    if (iswdigit(*line)) {
        ++line;
        while (isdigit(*line)) {
            ++line;
        }
        if (*line == L'.' && iswspace(*(line + 1))) {
            return true;
        }
    } else {
        if (*line == L'*' && iswspace(*(line + 1))) {
            return true;
        }
    }
    return false;
}



bool isCode(const wchar_t *line)
{
    if (*line == L'\t') {
        return true;
    }
    size_t spacesCount = 0;
    while (*line == ' ') {
        ++line;
        ++spacesCount;
    }
    return spacesCount > 3;
}



bool isHeader(const wchar_t *line)
{
    return *line == L'#';
}



bool isMinusLine(const wchar_t *line)
{
    if (*line == L'-') {
        while (*line == L'-') {
            ++line;
        }
        while (iswspace(*line)) {
            ++line;
        }
        return *line == L'\0';
    }
    return false;
}





TextViewer::TextViewer()
    : mCurrentParagraph(0)
    , mItems(0)
    , mCountItems(0)
{
}



TextViewer::~TextViewer()
{
    for (unsigned i = 0; i < mCountItems; ++i) {
        delete mItems[i];
    }
    delete[] mItems;
    delete mCurrentParagraph;
}



bool TextViewer::loadFromFile(const char *fileName)
{
    FILE *inputFile = fopen(fileName, "r");
    if (!inputFile) {
        fprintf(stderr, "Open file \"%s\" failed", fileName);
        return false;
    }

    unsigned fileLineNumber = 0;
    const wchar_t *currentLine = NULL;
    try {
        while (true) {
            ++fileLineNumber;
            currentLine = readLineAndAllocate(inputFile, fileLineNumber == 1);
            
            if (currentLine == NULL) {//Если чтение не удалось,
                addCurrentToItems();
                delete[] currentLine;
                break;//выходим из цикла
            }
            if (mCurrentParagraph == NULL) { //Если параграф не задан, то
                if (isEmptyString(currentLine)) {//Если строка пустая, то
                    delete[] currentLine;
                    continue; //ропускаем её
                }
                if (isList(currentLine)) {//Если это список, то
                    mCurrentParagraph = new BlockList();//создаем блок список
                } else {
                    if (isCode(currentLine)) {//Если это код, то
                        mCurrentParagraph = new BlockCode();//создаем блок кода
                    } else {
                        if (isHeader(currentLine)) {//Если это заголовок, то
                            mCurrentParagraph = new BlockHeader();//создаем блок заголовка
                            mCurrentParagraph->addTextLine(currentLine);
                            addCurrentToItems();
                        } else {
                            //Нужно проверить следующую строку на минусы.
                            const wchar_t *nextLine = readLineAndAllocate(inputFile);
                            ++fileLineNumber;
                            //Если строка успешно прочиталась и она является минус-строкой, то
                            if (nextLine != NULL && isMinusLine(nextLine)) {
                                //предыдущая строка является заголовком
                                mCurrentParagraph = new BlockHeader();//создаем блок заголовка
                                mCurrentParagraph->addTextLine(currentLine);
                                addCurrentToItems();
                                delete[] nextLine;
                            } else {
                                //Если ничего выше не сработало, значит это обычный блок абзаца
                                mCurrentParagraph = new BlockParagraph();//создаем блок абзаца
                                mCurrentParagraph->addTextLine(currentLine);
                                delete[] currentLine;                                
                                if (nextLine != NULL && isEmptyString(nextLine)) {
                                    delete[] nextLine;
                                    nextLine = NULL;
                                    addCurrentToItems();
                                }
                                currentLine = nextLine;
                            }
                        }
                    }
                }
            } else {
                if (isEmptyString(currentLine)) {
                    addCurrentToItems();
                }
            }
            if (mCurrentParagraph && currentLine) {
                mCurrentParagraph->addTextLine(currentLine);
            }
            delete[] currentLine;
            currentLine = NULL;
        }
        fclose(inputFile);
    } catch (const std::exception &e) {
        delete[] currentLine;
        fprintf(stderr, "Error in line (%u): %s", fileLineNumber, e.what()); 
        return false;
    } catch (...) {
        delete[] currentLine;
        fprintf(stderr, "Error in line (%u): %s", fileLineNumber, "unknown exception"); 
        return false;
    } 
    return true;
}



void TextViewer::print(bool isFile) const
{
    unsigned long int symbolsCount = 0;
    unsigned long int wordsCount = 0;
    for (unsigned i = 0; i < mCountItems; ++i) {
        mItems[i]->print(stdout);
        symbolsCount += mItems[i]->getCountSymbols();
        wordsCount += mItems[i]->getCountWords();
        if (!isFile) {
            fprintf(stdout, "\n");
        }
        fprintf(stdout, "\n");
    }
    fprintf(stderr, "\nsymbols: %lu, words: %lu\n", symbolsCount, wordsCount);
}



void TextViewer::addCurrentToItems()
{
    if (mCurrentParagraph != NULL) {
        AbstractText **newItems = new AbstractText*[mCountItems + 1];
        memcpy(newItems, mItems, mCountItems * sizeof(*mItems));
        delete[] mItems;
        mItems = newItems;
        mItems[mCountItems++] = mCurrentParagraph;
        mCurrentParagraph = NULL;
    }
}
